package main

import (
	"errors"
	"runtime"
	"sync/atomic"
	"unsafe"

	"gitea.com/anotherGo/alock"
)

type chunk struct {
	bytes []byte
	next  *chunk
	rPos  uint
}
type linkedbuffer struct {
	head      *chunk
	tail      *chunk
	writeLock alock.Lock
	totalLen  uint64
}

func (lb *linkedbuffer) append(turn *alock.Turn, clone bool, bytes [][]byte) {
	defer turn.Done()
	if len(bytes) == 0 {
		panic(errors.New("empty bytes"))
	}
	head := chunk{bytes: bytes[0]}
	tail := &head
	var totalLen uint64
	for _, byt := range bytes[1:] {
		if clone {
			tmp := make([]byte, len(byt))
			copy(tmp, byt)
			byt = tmp
		}
		totalLen += uint64(len(bytes))
		tail.next = &chunk{bytes: byt}
		tail = tail.next
	}
	turn.Wait()
	for {
		if CASchunks(lb.head, nil, &head) {
			break
		}
		CASchunks(lb.tail.next)
		if lb.tail != nil {
			if lb.tail.next == nil {
				lb.tail.next = lb.head
				break
			}
		}
		runtime.Gosched()
	}
	lb.tail = tail
	atomic.AddUint64()
}
func CASchunks(dst, old, new *chunk) bool {
	dstPtr := unsafe.Pointer(dst)
	return atomic.CompareAndSwapPointer(&dstPtr, unsafe.Pointer(old), unsafe.Pointer(new))
}
func main() {

}
